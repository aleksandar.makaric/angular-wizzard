import { AbstractControl } from '@angular/forms';

export const isInputInvalid = (control: AbstractControl | null) => control?.invalid
&& (control?.touched || control?.dirty);
