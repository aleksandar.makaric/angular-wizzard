import { Routes } from '@angular/router';

import { OffersComponent } from './pages';
import { OffersStepperComponent } from './pages/offers/offers-stepper/offers-stepper.component';
import { OffersTableComponent } from './pages/offers/offers-table/offers-table.component';

export const routes: Routes = [
  {
    path: 'offers',
    component: OffersComponent,
    children: [
      {
        path: 'table',
        component: OffersTableComponent,
        title: 'Created Offers',
      },
      {
        path: 'stepper/new/:id',
        component: OffersStepperComponent,
        title: 'New Offer',
      },
      {
        path: 'stepper/edit/:id',
        component: OffersStepperComponent,
        title: 'Edit Offer',
      },
      {
        path: 'stepper/preview/:id',
        component: OffersStepperComponent,
        title: 'Preview Offer',
      },
      { path: '', redirectTo: 'table', pathMatch: 'full' },
    ],
  },
  { path: '', redirectTo: 'offers', pathMatch: 'full' },
  { path: '**', redirectTo: 'offers', pathMatch: 'full' },
];
