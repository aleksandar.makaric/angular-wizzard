import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatStepper, MatStepperModule } from '@angular/material/stepper';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatIconModule } from '@angular/material/icon';
import { convertToString, isInputInvalid } from '../../../../../../../../../utils';
import {
  IConfiguration,
  IOffer,
  IOffersData,
  TConigurationStepsCompleted,
  THandleUpdateConfiguration,
  THandleUpdateConfigurationStepsCompleted,
  THandleUpdateStepper,
  TStepperMode,
} from '../../../../../../../../../types';

@Component({
  selector: 'app-step-one',
  standalone: true,
  imports: [
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatStepperModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatIconModule,
  ],
  templateUrl: './step-one.component.html',
  styleUrls: ['./step-one.component.css', '../steps.css'],
})
export class StepOneComponent implements OnInit {
  @Input() mode!: TStepperMode;
  @Input() offer!: IOffer;
  @Input() offersData!: IOffersData;
  @Input() configurationStepper!: MatStepper;
  @Input() handleUpdateStepper!: THandleUpdateStepper;
  @Input() handleUpdateStepsCompleted!: THandleUpdateConfigurationStepsCompleted;
  @Input() configuration?: IConfiguration;
  @Input() handleUpdateConfiguration!: THandleUpdateConfiguration;
  @Input() configurationStepsCompleted!: TConigurationStepsCompleted;

  constructor(private _formBuilder: FormBuilder) {}

  firstConfigurationStepForm = this._formBuilder.group({
    retirementPlanName: ['', Validators.required],
    retirementCategory: ['', Validators.required],
    retirementEntryAge: ['0', [Validators.required, Validators.min(20), Validators.max(130)]],
    retirementMinimumAge: ['0', [Validators.required, Validators.min(20), Validators.max(130)]],
    retirementMaximumAge: ['0', [Validators.required, Validators.min(20), Validators.max(130)]],
  });

  get retirementPlanName() {
    return this.firstConfigurationStepForm.get('retirementPlanName');
  }
  get retirementCategory() {
    return this.firstConfigurationStepForm.get('retirementCategory');
  }
  get retirementEntryAge() {
    return this.firstConfigurationStepForm.get('retirementEntryAge');
  }
  get retirementMinimumAge() {
    return this.firstConfigurationStepForm.get('retirementMinimumAge');
  }
  get retirementMaximumAge() {
    return this.firstConfigurationStepForm.get('retirementMaximumAge');
  }

  get isRetirementPlanNameInvalid() {
    return isInputInvalid(this.retirementPlanName);
  }
  get isRetirementCategoryInvalid() {
    return isInputInvalid(this.retirementCategory);
  }
  get isRetirementEntryAgeInvalid() {
    return isInputInvalid(this.retirementEntryAge);
  }
  get isRetirementMinimumAgeInvalid() {
    return isInputInvalid(this.retirementMinimumAge);
  }
  get isRetirementMaximumAgeInvalid() {
    return isInputInvalid(this.retirementMaximumAge);
  }

  get isNextButtonDisabled() {
    return (
      this.firstConfigurationStepForm.invalid || (this.mode === 'preview' && !this.configurationStepsCompleted.first)
    );
  }

  handleFirstConfigurationStepNext() {
    if ((this.mode === 'new' || this.mode === 'edit') && !this.firstConfigurationStepForm.disabled) {
      this.handleUpdateConfiguration({
        retirementPlanName: this.retirementPlanName!.value!,
        retirementCategory: Number(this.retirementCategory!.value!),
        retirementEntryAge: Number(this.retirementEntryAge!.value!),
        retirementMinimumAge: Number(this.retirementMinimumAge!.value!),
        retirementMaximumAge: Number(this.retirementMaximumAge!.value!),
      });
    }
    this.handleUpdateStepsCompleted({ ...this.configurationStepsCompleted, first: true });
    this.handleUpdateStepper();
    this.configurationStepper.next();
  }

  ngOnInit(): void {
    const retirementPlanName = this.offer?.details?.configuration?.retirementPlanName ?? '';
    const retirementCategory = convertToString(this.offer?.details?.configuration?.retirementCategory);
    const retirementEntryAge = convertToString(this.offer?.details?.configuration?.retirementEntryAge);
    const retirementMinimumAge = convertToString(this.offer?.details?.configuration?.retirementMinimumAge);
    const retirementMaximumAge = convertToString(this.offer?.details?.configuration?.retirementMaximumAge);

    this.firstConfigurationStepForm.patchValue({
      retirementPlanName,
      retirementCategory,
      retirementEntryAge,
      retirementMinimumAge,
      retirementMaximumAge,
    });

    if (
      retirementPlanName &&
      retirementCategory &&
      retirementEntryAge &&
      retirementMinimumAge &&
      retirementMaximumAge &&
      this.mode !== 'edit'
    ) {
      this.firstConfigurationStepForm.disable();

      this.handleUpdateStepsCompleted({
        first: true,
        second: false,
        third: false,
      });
    }

    if (this.mode === 'preview') {
      this.firstConfigurationStepForm.disable();
    }
  }
}
