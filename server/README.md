# BLKP Server

Simple Express.js server.

## Development server

Run `npm run start` for a dev server. Navigate to `http://localhost:3000/`. The application will automatically reload if you change any of the source files.

Run `npm run start:delay` to add delay to each response.