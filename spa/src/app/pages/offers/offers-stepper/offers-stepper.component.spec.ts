import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OffersStepperComponent } from './offers-stepper.component';

describe('OffersStepperComponent', () => {
  let component: OffersStepperComponent;
  let fixture: ComponentFixture<OffersStepperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [OffersStepperComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(OffersStepperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
