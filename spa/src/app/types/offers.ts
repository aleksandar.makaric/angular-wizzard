import { ISaving } from './offers-data';

export interface IOffer {
  id: number;
  createdDtm: string;
  modifiedDtm: string;
  status: TOfferStatus;
  details: IOfferDetails;
}

export type TOfferStatus =
  | 'INCOMPLETE'
  | 'PENDING'
  | 'CALCULATED'
  | 'IN REVIEW';

export interface IOfferDetails {
  foundation?: string;
  firstName?: string;
  lastName?: string;
  activeToRetiredRatio?: boolean;
  ageGroup?: boolean;
  admissionGuidelines?: boolean;
  retirementPlan?: string;
  useRetirementPlanTemplate?: boolean;
  configuration?: IConfiguration;
  calculations?: ICalulation[];
  assignee?: string;
}

export interface IConfiguration {
  retirementPlanName?: string;
  retirementCategory?: number;
  retirementEntryAge?: number;
  retirementMinimumAge?: number;
  retirementMaximumAge?: number;
  lumpSumDeathBenefit?: string;
  fundRates?: number;
  riskRatesCategory1?: number;
  savings?: ISaving[];
}

export interface ICalulation {
  label: string;
  currency: string;
  value: number;
}
