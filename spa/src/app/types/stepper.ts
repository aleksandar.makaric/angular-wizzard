import { IConfiguration, IOffer } from './offers';
import { IOffersData } from './offers-data';

export type TStepperMode = 'new' | 'edit' | 'preview';
export type TStepperSteps = 'first' | 'second' | 'third' | 'fourth';
export type TStepsCompleted = Record<TStepperSteps, boolean>;
export type TConfigurationSteps = 'first' | 'second' | 'third';
export type TConigurationStepsCompleted = Record<TConfigurationSteps, boolean>;
export type THandleUpdateStepper = () => void;
export type THandleUpdateOffer = (updatedOffer: IOffer) => void;
export type THandleUpdateOffersData = (updatedOffersData: IOffersData) => void;
export type THandleUpdateStepsCompleted = (updatedStepsCompleted: TStepsCompleted) => void;
export type THandleUpdateConfigurationStepsCompleted = (updatedStepsCompleted: TConigurationStepsCompleted) => void;
export type THandleUpdateConfiguration = (newConfiguration: IConfiguration) => void;
export type THandleConfigurationCompletion = (newOffersData: IOffersData, updatedOffer: IOffer) => void;
