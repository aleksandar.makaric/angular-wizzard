import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatStepper, MatStepperModule } from '@angular/material/stepper';
import { MatDialogRef } from '@angular/material/dialog';
import { switchMap } from 'rxjs';
import { OfferConfigurationComponent } from '../../offer-configuration.component';
import { OffersDataService, OffersService } from '../../../../../../../../../services';
import { convertToString, isInputInvalid } from '../../../../../../../../../utils';
import {
  IConfiguration,
  IOffer,
  IOffersData,
  TConigurationStepsCompleted,
  THandleConfigurationCompletion,
  THandleUpdateConfiguration,
  THandleUpdateConfigurationStepsCompleted,
  THandleUpdateStepper,
  TStepperMode,
} from '../../../../../../../../../types';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';

@Component({
  selector: 'app-step-three',
  standalone: true,
  imports: [
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatStepperModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatTableModule,
  ],
  templateUrl: './step-three.component.html',
  styleUrls: ['./step-three.component.css', '../steps.css'],
})
export class StepThreeComponent implements OnInit {
  @Input() mode!: TStepperMode;
  @Input() offer!: IOffer;
  @Input() offersData!: IOffersData;
  @Input() configurationStepper!: MatStepper;
  @Input() handleUpdateStepper!: THandleUpdateStepper;
  @Input() handleUpdateStepsCompleted!: THandleUpdateConfigurationStepsCompleted;
  @Input() configuration?: IConfiguration;
  @Input() handleUpdateConfiguration!: THandleUpdateConfiguration;
  @Input() configurationStepsCompleted!: TConigurationStepsCompleted;
  nextLoading = false;
  @Input() retirementPlan!: string;
  @Input() useRetirementPlanTemplate!: boolean;
  @Input() handleConfigurationCompletion!: THandleConfigurationCompletion;

  constructor(
    private _formBuilder: FormBuilder,
    private _offersService: OffersService,
    private _offersDataService: OffersDataService,
    private _dialogRef: MatDialogRef<OfferConfigurationComponent>
  ) {}

  thirdConfigurationStepForm = this._formBuilder.group({
    ageBracket: ['', [Validators.required, Validators.min(18), Validators.max(23)]],
    employee: ['', [Validators.required, Validators.min(0), Validators.max(100000)]],
    employer: ['', [Validators.required, Validators.min(0), Validators.max(100000)]],
    employeeDeduction: ['', [Validators.required, Validators.min(0), Validators.max(100000)]],
    employeeSupplement: ['', [Validators.required, Validators.min(0), Validators.max(100000)]],
  });

  get ageBracket() {
    return this.thirdConfigurationStepForm.get('ageBracket');
  }
  get employee() {
    return this.thirdConfigurationStepForm.get('employee');
  }
  get employer() {
    return this.thirdConfigurationStepForm.get('employer');
  }
  get employeeDeduction() {
    return this.thirdConfigurationStepForm.get('employeeDeduction');
  }
  get employeeSupplement() {
    return this.thirdConfigurationStepForm.get('employeeSupplement');
  }

  get isAgeBracketInvalid() {
    return isInputInvalid(this.ageBracket);
  }
  get isEmployeeInvalid() {
    return isInputInvalid(this.employee);
  }
  get isEmployerInvalid() {
    return isInputInvalid(this.employer);
  }
  get isEmployeeDeductionInvalid() {
    return isInputInvalid(this.employeeDeduction);
  }
  get isEmployeeSupplementInvalid() {
    return isInputInvalid(this.employeeSupplement);
  }

  displayedColumns: string[] = ['ageBracket', 'employee', 'employer', 'employeeDeduction', 'employeeSupplement'];

  handleThirdSubStepNext(): void {
    if ((this.mode === 'new' || this.mode === 'edit') && !this.thirdConfigurationStepForm.disabled) {
      let updatedConfiguration: IConfiguration;

      if (this.thirdConfigurationStepForm.valid) {
        updatedConfiguration = {
          ...this.configuration,
          savings: [
            {
              ageBracket: Number(this.ageBracket!.value),
              employee: Number(this.employee!.value),
              employer: Number(this.employer!.value),
              employeeDeduction: Number(this.employeeDeduction!.value),
              employeeSupplement: Number(this.employeeSupplement!.value),
            },
          ],
        }
        this.handleUpdateConfiguration(updatedConfiguration);
      } else {
        updatedConfiguration = { ...this.configuration };
        delete updatedConfiguration.savings;
        this.handleUpdateConfiguration(updatedConfiguration);
      }

      this.nextLoading = true;
      this.thirdConfigurationStepForm.disable();
      this._offersService
        .updateOffer({
          ...this.offer!,
          details: {
            ...(this.offer?.details ? this.offer.details : {}),
            retirementPlan: this.retirementPlan,
            useRetirementPlanTemplate: this.useRetirementPlanTemplate,
            configuration: updatedConfiguration,
          },
        })
        .pipe(
          switchMap((updatedOffer) => {
            this.offer = updatedOffer;
            return this._offersDataService.getOffersData();
          })
        )
        .subscribe((newOffersData) => {
          this.nextLoading = false;
          this.thirdConfigurationStepForm.enable();
          this.handleUpdateStepsCompleted({ ...this.configurationStepsCompleted, third: true });
          this.handleUpdateStepper();
          this._dialogRef.close();
          this.handleConfigurationCompletion(newOffersData, this.offer);
        });
    } else {
      this.handleUpdateStepsCompleted({ ...this.configurationStepsCompleted, third: true });
      this.handleUpdateStepper();
      this._dialogRef.close();
    }
  }

  ngOnInit(): void {
    if (this.offer?.details?.configuration?.savings?.length || this?.offersData?.savings?.length) {
      this.thirdConfigurationStepForm.patchValue({
        ageBracket:
          convertToString(this.offer?.details?.configuration?.savings?.[0]?.ageBracket) ||
          convertToString(this.offersData?.savings?.[0]?.ageBracket),
        employee:
          convertToString(this.offer?.details?.configuration?.savings?.[0]?.employee) ||
          convertToString(this.offersData?.savings?.[0]?.employee),
        employer:
          convertToString(this.offer?.details?.configuration?.savings?.[0]?.employer) ||
          convertToString(this.offersData?.savings?.[0]?.employer),
        employeeDeduction:
          convertToString(this.offer?.details?.configuration?.savings?.[0]?.employeeDeduction) ||
          convertToString(this.offersData?.savings?.[0]?.employeeDeduction),
        employeeSupplement:
          convertToString(this.offer?.details?.configuration?.savings?.[0]?.employeeSupplement) ||
          convertToString(this.offersData?.savings?.[0]?.employeeSupplement),
      });
    }

    if (this.mode === 'preview') {
      this.thirdConfigurationStepForm.disable();

      this.handleUpdateStepsCompleted({
        first: true,
        second: true,
        third: true,
      });
    }
  }
}
