import { Component, Input, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { BreakpointObserver } from '@angular/cdk/layout';
import {
  StepperOrientation,
  MatStepperModule,
  MatStepper,
} from '@angular/material/stepper';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { ConfirmationDialogComponent } from '../../../../components';
import { StepOneComponent } from './steps/step-one/step-one.component';
import { StepTwoComponent } from './steps/step-two/step-two.component';
import { StepThreeComponent } from './steps/step-three/step-three.component';
import { StepFourComponent } from './steps/step-four/step-four.component';
import { DialogService } from '../../../../services';
import { AsyncPipe } from '@angular/common';
import {
  IOffer,
  IOffersData,
  THandleUpdateOffer,
  THandleUpdateOffersData,
  THandleUpdateStepper,
  THandleUpdateStepsCompleted,
  TStepperMode,
  TStepsCompleted,
} from '../../../../types';

@Component({
  selector: 'app-stepper',
  standalone: true,
  imports: [
    MatStepperModule,
    MatButtonModule,
    MatIconModule,
    MatTooltipModule,
    AsyncPipe,
    StepOneComponent,
    StepTwoComponent,
    StepThreeComponent,
    StepFourComponent,
  ],
  templateUrl: './stepper.component.html',
  styleUrl: './stepper.component.css',
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: { appearance: 'outline' },
    },
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: { displayDefaultIndicatorType: false },
    },
  ]
})
export class StepperComponent {
  @ViewChild('stepper') stepper!: MatStepper;
  @Input() id!: number;
  @Input() mode!: TStepperMode;
  @Input() completionMessage!: string;
  @Input() offer!: IOffer;
  @Input() offersData!: IOffersData;
  stepsCompleted: TStepsCompleted = {
    first: false,
    second: false,
    third: false,
    fourth: false,
  };
  stepNextLoading = false;
  stepBackLoading = false;
  stepperOrientation: Observable<StepperOrientation>;
  private _root: HTMLElement;
  fullScreen = false;

  handleUpdateStepper: THandleUpdateStepper = () => {
    this.stepper.selected!.completed = true;
  };
  handleUpdateOffer: THandleUpdateOffer = (updatedOffer) => {
    this.offer = updatedOffer;
  };
  handleUpdateOffersData: THandleUpdateOffersData = (updatedOffersData) => {
    this.offersData = updatedOffersData;
  };
  handleUpdateStepsCompleted: THandleUpdateStepsCompleted = (
    updatedStepsCompleted
  ) => {
    this.stepsCompleted = updatedStepsCompleted;
  };

  constructor(
    private _router: Router,
    breakpointObserver: BreakpointObserver,
    private _dialogService: DialogService
  ) {
    this.stepperOrientation = breakpointObserver
      .observe('(min-width: 800px)')
      .pipe(map(({ matches }) => (matches ? 'horizontal' : 'vertical')));

    this._root = document.documentElement;
  }

  isFullScreenEnabled = () => document.fullscreenEnabled;

  toggleFullScreen = () => {
    if (document.fullscreenElement) {
      document.exitFullscreen();
      this.fullScreen = false;
    } else {
      this._root.requestFullscreen();
      this.fullScreen = true;
    }
  };

  handleCloseStepper = () => {
    if (this.mode !== 'preview') {
      this._dialogService.handleDialogOpen(ConfirmationDialogComponent, {
        data: {
          title: 'Confirm Leaving Offer Stepper',
          message:
            'Are you sure you want to leave the offer stepper? The progress you made might not be saved.',
        },
        width: '300px',
      });
      const onDialogClose = (confirmed: boolean) => {
        if (confirmed) {
          this._router.navigate(['/offers/table']);
        }
      };
      this._dialogService.handleDialogClose(onDialogClose);
    } else {
      this._router.navigate(['/offers/table']);
    }
  };
}
