import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectChange, MatSelectModule } from '@angular/material/select';
import { MatStepper, MatStepperModule } from '@angular/material/stepper';
import { Subscription, switchMap } from 'rxjs';
import { OfferConfigurationComponent } from './offer-configuration/offer-configuration.component';
import { DialogService, OffersDataService, OffersService } from '../../../../../../services';
import { isInputInvalid } from '../../../../../../utils';
import {
  IOffer,
  IOffersData,
  THandleConfigurationCompletion,
  THandleUpdateOffer,
  THandleUpdateOffersData,
  THandleUpdateStepper,
  THandleUpdateStepsCompleted,
  TStepperMode,
  TStepsCompleted,
} from '../../../../../../types';

@Component({
  selector: 'app-step-two',
  standalone: true,
  imports: [
    MatStepperModule,
    MatFormFieldModule,
    MatSelectModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  templateUrl: './step-two.component.html',
  styleUrls: ['./step-two.component.css', '../steps.css'],
})
export class StepTwoComponent implements OnInit, OnDestroy {
  @Input() stepper!: MatStepper;
  @Input() handleUpdateStepper!: THandleUpdateStepper;
  @Input() mode!: TStepperMode;
  @Input() offer!: IOffer;
  @Input() handleUpdateOffer!: THandleUpdateOffer;
  @Input() offersData!: IOffersData;
  @Input() handleUpdateOffersData!: THandleUpdateOffersData;
  nextLoading = false;
  @Input() stepsCompleted!: TStepsCompleted;
  @Input() handleUpdateStepsCompleted!: THandleUpdateStepsCompleted;
  private _subscriptions: Subscription[] = [];

  constructor(
    private _formBuilder: FormBuilder,
    private _offersService: OffersService,
    private _offersDataService: OffersDataService,
    private _dialogService: DialogService
  ) {}

  secondStepForm = this._formBuilder.group({
    retirementPlan: ['', Validators.required],
    useRetirementPlanTemplate: [false],
  });

  get retirementPlan() {
    return this.secondStepForm.get('retirementPlan');
  }
  get useRetirementPlanTemplate() {
    return this.secondStepForm.get('useRetirementPlanTemplate');
  }
  get isRetirementPlanInvalid() {
    return isInputInvalid(this.retirementPlan);
  }
  get isNextButtonDisabled() {
    return this.secondStepForm.invalid || (this.mode === 'preview' && !this.stepsCompleted.second);
  }

  handleRetirementPlanSelect($event: MatSelectChange) {
    if ($event.value) {
      this.useRetirementPlanTemplate?.enable();
    }
  }

  handleSecondStepSubmit(): void {
    if (this.secondStepForm.valid && (this.mode === 'new' || this.mode === 'edit') && !this.secondStepForm.disabled) {
      this.nextLoading = true;
      // Enable in error case
      this.secondStepForm.disable();

      const newOffer = { ...this.offer };

      if (newOffer?.details?.configuration) {
        delete newOffer.details.configuration;
      }

      const offersSubscription = this._offersService
        .updateOffer({
          ...newOffer!,
          details: {
            ...newOffer?.details,
            retirementPlan: this.secondStepForm.value.retirementPlan!,
            useRetirementPlanTemplate: this.secondStepForm.value.useRetirementPlanTemplate!,
          },
        })
        .pipe(
          switchMap((updatedOffer) => {
            this.handleUpdateOffer(updatedOffer);
            return this._offersDataService.getOffersData();
          })
        )
        .subscribe((newOffersData) => {
          this.nextLoading = false;
          this.offersData = newOffersData;
          this.handleUpdateStepsCompleted({
            ...this.stepsCompleted,
            second: true,
            third: true,
          });
          this.handleUpdateStepper();
          this.stepper.next();
        });
      this._subscriptions.push(offersSubscription);
    } else {
      this.handleUpdateStepsCompleted({
        ...this.stepsCompleted,
        second: true,
        third: true,
      });
      this.handleUpdateStepper();
      this.stepper.next();
    }
  }

  private _handleConfigurationCompletion: THandleConfigurationCompletion = (newOffersData, updatedOffer) => {
    this.secondStepForm.disable();
    this.handleUpdateOffersData(newOffersData);
    this.handleUpdateOffer(updatedOffer);
    this.handleUpdateStepsCompleted({
      ...this.stepsCompleted,
      second: true,
      third: true,
    });
    this.stepper.selected!.completed = true;
    this.stepper.next();
  };

  handleSubstepperOpen = () => {
    this._dialogService.handleDialogOpen(OfferConfigurationComponent, {
      maxWidth: '1200px',
      width: '95%',
      data: {
        mode: this.mode,
        offer: this.offer,
        offersData: this.offersData,
        handleConfigurationCompletion: this._handleConfigurationCompletion,
        retirementPlan: this.retirementPlan!.value,
        useRetirementPlanTemplate: true,
      },
    });
  };

  ngOnInit(): void {
    const retirementPlan = this.offer?.details?.retirementPlan;
    const useRetirementPlanTemplate = this.offer?.details?.useRetirementPlanTemplate;

    this.secondStepForm.patchValue({
      retirementPlan: retirementPlan ?? '',
      useRetirementPlanTemplate: useRetirementPlanTemplate ?? false,
    });
    if (retirementPlan && this.mode !== 'edit') {
      this.secondStepForm.disable();

      this.handleUpdateStepsCompleted({
        ...this.stepsCompleted,
        second: true,
        third: true,
      });
    }
    if (!retirementPlan) {
      this.useRetirementPlanTemplate?.disable();
    }

    if (this.mode === 'preview') {
      this.secondStepForm.disable();
    }
  }

  ngOnDestroy(): void {
    this._subscriptions.forEach((subscription) => subscription.unsubscribe());
    this._dialogService.handleDialogUnsubscribe();
  }
}
