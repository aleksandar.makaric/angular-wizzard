import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AsyncPipe } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatIconModule } from '@angular/material/icon';
import { MatDialog } from '@angular/material/dialog';
import { Subscription, forkJoin } from 'rxjs';
import { StepperComponent } from './stepper/stepper.component';
import { SpinnerComponent } from '../../../components/spinner/spinner.component';
import { ConfirmationDialogComponent } from '../../../components/confirmation-dialog/confirmation-dialog.component';
import { ToNumberPipe } from '../../../pipes';
import {
  DialogService,
  OffersDataService,
  OffersService,
} from '../../../services';
import { IOffer, IOffersData, TStepperMode } from '../../../types';

@Component({
  selector: 'app-offers-stepper',
  standalone: true,
  imports: [
    AsyncPipe,
    MatButtonModule,
    StepperComponent,
    SpinnerComponent,
    MatProgressSpinnerModule,
    MatIconModule,
    ToNumberPipe,
  ],
  templateUrl: './offers-stepper.component.html',
  styleUrl: './offers-stepper.component.css',
})
export class OffersStepperComponent implements OnInit, OnDestroy {
  id?: string | null;
  mode?: TStepperMode;
  title?: string;
  completionMessage?: string;
  showOfferDeleteButton?: boolean;
  offer?: IOffer;
  offersData?: IOffersData;
  offersLoading = true;
  offerDeletionLoading = false;
  private _subscriptions: Subscription[] = [];

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    public dialog: MatDialog,
    private _offersService: OffersService,
    private _offersDataService: OffersDataService,
    private _dialogService: DialogService
  ) {}

  handleEditOffer() {
    this._router.navigate(['/offers/stepper/edit', this.id]);
  }

  handleClosePreviewOffer() {
    this._router.navigate(['/offers/table']);
  }

  handleDeleteOffer = () => {
    this._dialogService.handleDialogOpen(ConfirmationDialogComponent, {
      data: {
        title: 'Delete Offer',
        message: `Would you like to delete Offer ${this.id}?`,
      },
      width: '300px',
    });

    const onDialogClose = (confirmed: boolean) => {
      if (confirmed) {
        this.offerDeletionLoading = true;
        const deleteOfferSubscription = this._offersService
          .deleteOffer(Number(this.id))
          .subscribe(() => {
            this.offerDeletionLoading = false;
            this._router.navigate(['/offers/table']);
          });

        this._subscriptions.push(deleteOfferSubscription);
      }
    };
    this._dialogService.handleDialogClose(onDialogClose);
  };

  ngOnInit(): void {
    const routeSubscription = this._route.url.subscribe((segments) => {
      this.mode = segments[1].path as TStepperMode;
    });
    this._subscriptions.push(routeSubscription);

    this.id = this._route.snapshot.paramMap.get('id');

    if (!this.id || !this.mode) {
      this._router.navigate(['/offers/table']);
    }

    const offersSubscription = forkJoin([
      this._offersService.getOffer(Number(this.id)),
      this._offersDataService.getOffersData(),
    ]).subscribe(([offersResponse, offersDataResponse]) => {
      this.offer = offersResponse;
      this.offersData = offersDataResponse;
      this.offersLoading = false;
    });
    this._subscriptions.push(offersSubscription);

    if (this.mode === 'new') {
      this.title = `New Offer ${this.id}`;
      this.showOfferDeleteButton = true;
      this.completionMessage = 'Offer created successfully!';
    }
    if (this.mode === 'edit') {
      this.title = `Edit Offer ${this.id}`;
      this.showOfferDeleteButton = true;
      this.completionMessage = 'Offer updated successfully!';
    }
    if (this.mode === 'preview') {
      this.title = `Preview Offer ${this.id}`;
      this.showOfferDeleteButton = false;
    }
  }

  ngOnDestroy(): void {
    this._subscriptions.forEach((subscription) => subscription.unsubscribe());

    this._dialogService.handleDialogUnsubscribe();
  }
}
