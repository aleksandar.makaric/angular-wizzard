import { AsyncPipe, DatePipe } from '@angular/common';
import {
  AfterViewInit, Component, OnDestroy, ViewChild,
} from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatDialog } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSort, MatSortModule } from '@angular/material/sort';
import { MatTable, MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { Subscription, switchMap } from 'rxjs';

import { ConfirmationDialogComponent } from '../../../components/confirmation-dialog/confirmation-dialog.component';
import { SpinnerComponent } from '../../../components/spinner/spinner.component';
import { DialogService, OffersService, SnackbarService } from '../../../services';
import { IOffer } from '../../../types';
import { OffersTableDataSource } from './offers-table-datasource';

@Component({
  selector: 'app-offers-table',
  templateUrl: './offers-table.component.html',
  styleUrls: ['./offers-table.component.css'],
  standalone: true,
  imports: [
    AsyncPipe,
    DatePipe,
    MatButtonModule,
    MatChipsModule,
    MatIconModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    RouterModule,
    SpinnerComponent,
  ],
})
export class OffersTableComponent implements AfterViewInit, OnDestroy {
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  @ViewChild(MatSort) sort!: MatSort;

  @ViewChild(MatTable) table!: MatTable<IOffer>;

  dataSource = new OffersTableDataSource(this._offersService);

  tableLoading = true;

  newOfferLoading = false;

  deleteLoading: number[] = [];

  private _subscriptions: Subscription[] = [];

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    public dialog: MatDialog,
    private _offersService: OffersService,
    private _dialogService: DialogService,
    private _snackbarService: SnackbarService,
  ) {}

  displayedColumns = ['id', 'modifiedDtm', 'name', 'status', 'actions'];

  handleNewOffer() {
    this.newOfferLoading = true;
    const createOfferSubscription = this._offersService.createOffer().subscribe((offersResponse) => {
      this.newOfferLoading = false;
      this._router.navigate([`../stepper/new/${offersResponse.id}`], {
        relativeTo: this._route,
      });
    });
    this._subscriptions.push(createOfferSubscription);
  }

  handlePreviewOffer(id: number) {
    this._router.navigate([`../stepper/preview/${id}`], {
      relativeTo: this._route,
    });
  }

  handleEditOffer(id: number) {
    this._router.navigate([`../stepper/edit/${id}`], {
      relativeTo: this._route,
    });
  }

  handleDeleteOffer(id: number): void {
    this._dialogService.handleDialogOpen(ConfirmationDialogComponent, {
      data: {
        title: 'Delete Offer',
        message: `Would you like to delete Offer ${id}?`,
      },
      width: '300px',
    });

    const onDialogClose = (confirmed: boolean) => {
      if (confirmed) {
        this.deleteLoading = [...this.deleteLoading, id];
        const deleteOfferSubscription = this._offersService
          .deleteOffer(id)
          .pipe(switchMap(() => this._offersService.getOffers()))
          .subscribe(() => {
            this.deleteLoading = this.deleteLoading.filter((dL) => dL !== id);
            this._snackbarService.handleOpenSnackbar(`Successfully deleted offer ${id}.`, 'Close', 'success');
          });
        this._subscriptions.push(deleteOfferSubscription);
      }
    };
    this._dialogService.handleDialogClose(onDialogClose);
  }

  isDeleteLoading = (id: number) => this.deleteLoading.includes(id);

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
    this.dataSource.loading$.subscribe((loading) => (this.tableLoading = loading));
  }

  ngOnDestroy(): void {
    this._subscriptions.forEach((subscription) => subscription.unsubscribe());

    this._dialogService.handleDialogUnsubscribe();
  }
}
