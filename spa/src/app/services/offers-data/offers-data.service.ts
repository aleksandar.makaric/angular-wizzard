import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject, tap } from 'rxjs';

import { environment } from '../../../../environments/environment';
import { IOffersData } from '../../types';

@Injectable({
  providedIn: 'root',
})
export class OffersDataService {
  private _endpoint = `${environment.baseUrl}/offers-data`;

  private _offersData$ = new Subject<IOffersData>();

  get offersData() {
    return this._offersData$.asObservable();
  }

  constructor(private _http: HttpClient) {}

  getOffersData() {
    return this._http.get<IOffersData>(this._endpoint).pipe(
      tap((offers) => {
        this._offersData$.next(offers);
      }),
    );
  }
}
