import { Component, Input } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { MatTableModule } from '@angular/material/table';
import { IOffer } from '../../../../../../../types';

@Component({
  selector: 'app-calculation-table',
  standalone: true,
  imports: [MatTableModule, DecimalPipe],
  templateUrl: './calculation-table.component.html',
  styleUrl: './calculation-table.component.css',
})
export class CalculationTableComponent {
  @Input() offer?: IOffer;
  displayedColumns: string[] = ['costs', 'currency', 'amount'];
}
