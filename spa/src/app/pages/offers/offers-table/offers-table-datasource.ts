import { DataSource } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { map } from 'rxjs/operators';
import { Observable, merge, BehaviorSubject, Subscription } from 'rxjs';
import { OffersService } from '../../../services/offers/offers.service';
import { IOffer } from '../../../types';

export class OffersTableDataSource extends DataSource<IOffer> {
  data$ = new BehaviorSubject<IOffer[]>([]);
  paginator: MatPaginator | undefined;
  sort: MatSort | undefined;
  loading$ = new BehaviorSubject<boolean>(true);
  private _getOffersSubscription: Subscription;

  constructor(private _offersService: OffersService) {
    super();
    this._getOffersSubscription = this._offersService.offers.subscribe(
      (offers) => {
        this.data$.next(offers);
      }
    );
    this._handleGetOffers();
  }

  private _handleGetOffers() {
    this._offersService.getOffers().subscribe(() => {
      this.loading$.next(false);
    });
  }

  connect(): Observable<IOffer[]> {
    if (this.paginator && this.sort) {
      return merge(this.data$, this.paginator.page, this.sort.sortChange).pipe(
        map(() => {
          return this.getPagedData(this.getSortedData([...this.data$.value]));
        })
      );
    } else {
      throw Error(
        'Please set the paginator and sort on the data source before connecting.'
      );
    }
  }

  disconnect(): void {
    this.data$.complete();
    this._getOffersSubscription.unsubscribe()
  }

  private getPagedData(data: IOffer[]): IOffer[] {
    if (this.paginator) {
      const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
      return data.splice(startIndex, this.paginator.pageSize);
    } else {
      return data;
    }
  }

  private getSortedData(data: IOffer[]): IOffer[] {
    if (!this.sort || !this.sort.active || this.sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = this.sort?.direction === 'asc';
      switch (this.sort?.active) {
        case 'id':
          return compare(+a.id, +b.id, isAsc);
        case 'modifiedDtm':
          return compare(
            new Date(a.modifiedDtm),
            new Date(b.modifiedDtm),
            isAsc
          );
        case 'name':
          return compare(
            `${a.details?.firstName} ${a.details?.lastName}`,
            `${b.details?.firstName} ${b.details?.lastName}`,
            isAsc
          );
        case 'status':
          return compare(a.status, b.status, isAsc);
        default:
          return 0;
      }
    });
  }
}

function compare(
  a: string | Date | number,
  b: string | Date | number,
  isAsc: boolean
): number {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
