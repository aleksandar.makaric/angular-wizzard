import { Component, Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogContent, MatDialogTitle } from '@angular/material/dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldModule } from '@angular/material/form-field';
import { MatStepper, MatStepperModule } from '@angular/material/stepper';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { StepOneComponent } from './steps/step-one/step-one.component';
import { StepTwoComponent } from './steps/step-two/step-two.component';
import { StepThreeComponent } from './steps/step-three/step-three.component';
import {
  IConfiguration,
  IOffer,
  IOffersData,
  THandleUpdateConfiguration,
  THandleUpdateConfigurationStepsCompleted,
  THandleUpdateStepper,
  TStepperMode,
} from '../../../../../../../types';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';

interface IDialogData {
  mode: TStepperMode;
  offer: IOffer;
  offersData: IOffersData;
  handleConfigurationCompletion: (newOffersData: IOffersData, updatedOffer: IOffer) => void;
  retirementPlan: string;
  useRetirementPlanTemplate: boolean;
}

@Component({
  selector: 'app-offer-configuration',
  standalone: true,
  imports: [
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDialogTitle,
    MatDialogContent,
    MatStepperModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatTableModule,
    StepOneComponent,
    StepTwoComponent,
    StepThreeComponent
  ],
  templateUrl: './offer-configuration.component.html',
  styleUrl: './offer-configuration.component.css',
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: { appearance: 'outline' },

    },
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: { displayDefaultIndicatorType: false },
    },
  ],
})
export class OfferConfigurationComponent {
  @ViewChild('configurationStepper') configurationStepper!: MatStepper;
  mode: TStepperMode;
  offer: IOffer;
  offersData: IOffersData;
  retirementPlan;
  useRetirementPlanTemplate;
  handleConfigurationCompletion;
  substepNextLoading = false;
  substepBackLoading = false;
  configurationStepsCompleted = {
    first: false,
    second: false,
    third: false,
  };
  configuration?: IConfiguration;

  handleUpdateStepper: THandleUpdateStepper = () => {
    this.configurationStepper.selected!.completed = true;
  };

  handleUpdateStepsCompleted: THandleUpdateConfigurationStepsCompleted = (updatedStepsCompleted) => {
    this.configurationStepsCompleted = updatedStepsCompleted;
  };

  handleUpdateConfiguration: THandleUpdateConfiguration = (newConfiguration) => {
    this.configuration = newConfiguration;
  };

  constructor(@Inject(MAT_DIALOG_DATA) private _dialogData: IDialogData) {
    this.mode = this._dialogData.mode;
    this.offer = this._dialogData.offer;
    this.offersData = this._dialogData.offersData;
    this.retirementPlan = this._dialogData.retirementPlan;
    this.useRetirementPlanTemplate = this._dialogData.useRetirementPlanTemplate;
    this.handleConfigurationCompletion = this._dialogData.handleConfigurationCompletion;
  }
}
