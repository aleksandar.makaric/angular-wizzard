import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatIconModule } from '@angular/material/icon';
import {
  IConfiguration,
  IOffer,
  IOffersData,
  TConigurationStepsCompleted,
  THandleUpdateConfiguration,
  THandleUpdateConfigurationStepsCompleted,
  THandleUpdateStepper,
  TStepperMode,
} from '../../../../../../../../../types';
import { MatStepper, MatStepperModule } from '@angular/material/stepper';
import { convertToString, isInputInvalid } from '../../../../../../../../../utils';

@Component({
  selector: 'app-step-two',
  standalone: true,
  imports: [
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatStepperModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatIconModule,
  ],
  templateUrl: './step-two.component.html',
  styleUrls: ['./step-two.component.css', '../steps.css'],
})
export class StepTwoComponent implements OnInit {
  @Input() mode!: TStepperMode;
  @Input() offer!: IOffer;
  @Input() offersData!: IOffersData;
  @Input() configurationStepper!: MatStepper;
  @Input() handleUpdateStepper!: THandleUpdateStepper;
  @Input() handleUpdateStepsCompleted!: THandleUpdateConfigurationStepsCompleted;
  @Input() configuration?: IConfiguration;
  @Input() handleUpdateConfiguration!: THandleUpdateConfiguration;
  @Input() configurationStepsCompleted!: TConigurationStepsCompleted;

  constructor(private _formBuilder: FormBuilder) {}

  private _initialSecondConfigurationStepFormValues = {
    lumpSumDeathBenefit: '',
    fundRates: '0',
    riskRatesCategory1: '0',
  };

  secondConfigurationStepForm = this._formBuilder.group({
    lumpSumDeathBenefit: [this._initialSecondConfigurationStepFormValues.lumpSumDeathBenefit],
    fundRates: [
      this._initialSecondConfigurationStepFormValues.fundRates,
      [Validators.required, Validators.min(0), Validators.max(1000)],
    ],
    riskRatesCategory1: [
      this._initialSecondConfigurationStepFormValues.riskRatesCategory1,
      [Validators.required, Validators.min(0), Validators.max(1000)],
    ],
  });

  get lumpSumDeathBenefit() {
    return this.secondConfigurationStepForm.get('lumpSumDeathBenefit');
  }
  get fundRates() {
    return this.secondConfigurationStepForm.get('fundRates');
  }
  get riskRatesCategory1() {
    return this.secondConfigurationStepForm.get('riskRatesCategory1');
  }

  get isFundRatesInvalid() {
    return isInputInvalid(this.fundRates);
  }
  get isRiskRatesCategory1Invalid() {
    return isInputInvalid(this.riskRatesCategory1);
  }

  handleSecondSubStepNext(): void {
    if ((this.mode === 'new' || this.mode === 'edit') && !this.secondConfigurationStepForm.disabled) {
      if (this.secondConfigurationStepForm.valid) {
        this.handleUpdateConfiguration({
          ...this.configuration,
          ...(this.lumpSumDeathBenefit!.value ? { lumpSumDeathBenefit: this.lumpSumDeathBenefit!.value } : {}),
          fundRates: Number(this.fundRates!.value),
          riskRatesCategory1: Number(this.riskRatesCategory1!.value),
        });
      } else {
        const newConfiguration = { ...this.configuration };
        delete newConfiguration.lumpSumDeathBenefit;
        delete newConfiguration.fundRates;
        delete newConfiguration.riskRatesCategory1;
        this.handleUpdateConfiguration(newConfiguration);
      }
    }

    this.handleUpdateStepsCompleted({ ...this.configurationStepsCompleted, second: true });
    this.handleUpdateStepper();
    this.configurationStepper.next();
  }

  ngOnInit(): void {
    const lumpSumDeathBenefit = this.offer?.details?.configuration?.lumpSumDeathBenefit ?? '';
    const fundRates = convertToString(this.offer?.details?.configuration?.fundRates);
    const riskRatesCategory1 = convertToString(this.offer?.details?.configuration?.riskRatesCategory1);

    this.secondConfigurationStepForm.patchValue({
      lumpSumDeathBenefit,
      fundRates,
      riskRatesCategory1,
    });

    if (this.mode === 'preview') {
      this.secondConfigurationStepForm.disable();

      this.handleUpdateStepsCompleted({
        first: true,
        second: true,
        third: false,
      });
    }
  }
}
