import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatStepper, MatStepperModule } from '@angular/material/stepper';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatButtonModule } from '@angular/material/button';
import { Subscription, switchMap } from 'rxjs';
import { OffersDataService, OffersService, SnackbarService } from '../../../../../../services';
import { isInputInvalid } from '../../../../../../utils';
import {
  IOffer,
  IOffersData,
  THandleUpdateOffer,
  THandleUpdateOffersData,
  THandleUpdateStepper,
  THandleUpdateStepsCompleted,
  TStepperMode,
  TStepsCompleted,
} from '../../../../../../types';

@Component({
  selector: 'app-step-four',
  standalone: true,
  imports: [
    MatStepperModule,
    MatFormFieldModule,
    MatSelectModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  templateUrl: './step-four.component.html',
  styleUrls: ['./step-four.component.css', '../steps.css'],
})
export class StepFourComponent implements OnInit, OnDestroy {
  @Input() stepper!: MatStepper;
  @Input() handleUpdateStepper!: THandleUpdateStepper;
  @Input() mode!: TStepperMode;
  @Input() offer!: IOffer;
  @Input() handleUpdateOffer!: THandleUpdateOffer;
  @Input() offersData!: IOffersData;
  @Input() handleUpdateOffersData!: THandleUpdateOffersData;
  @Input() stepsCompleted!: TStepsCompleted;
  @Input() handleUpdateStepsCompleted!: THandleUpdateStepsCompleted;
  @Input() completionMessage!: string;
  nextLoading = false;
  private _subscriptions: Subscription[] = [];

  constructor(
    private _formBuilder: FormBuilder,
    private _router: Router,
    private _offersService: OffersService,
    private _offersDataService: OffersDataService,
    private _snackbarService: SnackbarService
  ) {}

  fourthStepForm = this._formBuilder.group({
    assignee: ['', Validators.required],
  });

  get assignee() {
    return this.fourthStepForm.get('assignee');
  }
  get isAssigneeInvalid() {
    return isInputInvalid(this.assignee);
  }

  handleFourthStepSubmit(): void {
    if (this.fourthStepForm.valid && (this.mode === 'new' || this.mode === 'edit') && !this.fourthStepForm.disabled) {
      this.nextLoading = true;
      // Enable in case of error
      this.fourthStepForm.disable();
      const offersSubscription = this._offersService
        .updateOffer({
          ...this.offer!,
          details: {
            ...this.offer?.details,
            assignee: this.fourthStepForm.value.assignee!,
          },
        })
        .pipe(
          switchMap((updatedOffer) => {
            this.handleUpdateOffer(updatedOffer);
            return this._offersDataService.getOffersData();
          })
        )
        .subscribe((newOffersData) => {
          this.nextLoading = false;
          this.handleUpdateOffersData(newOffersData);
          this.handleUpdateStepsCompleted({
            ...this.stepsCompleted,
            fourth: true,
          });
          this.handleUpdateStepper();
          this._snackbarService.handleOpenSnackbar(this.completionMessage!, 'Close', 'success');
          this._router.navigate(['/offers/table']);
        });
      this._subscriptions.push(offersSubscription);
    } else {
      this.handleUpdateStepsCompleted({ ...this.stepsCompleted, fourth: true });
      this.handleUpdateStepper();
      this._router.navigate(['/offers/table']);
    }
  }

  ngOnInit(): void {
    const assignee = this.offer?.details?.assignee;

    this.fourthStepForm.patchValue({
      assignee: assignee ?? '',
    });

    if (assignee && this.mode !== 'edit') {
      this.fourthStepForm.disable();
      this.handleUpdateStepsCompleted({ ...this.stepsCompleted, fourth: true });
    }

    if (this.mode === 'preview') {
      this.fourthStepForm.disable();
    }
  }

  ngOnDestroy(): void {
    this._subscriptions.forEach((subscription) => subscription.unsubscribe());
  }
}
