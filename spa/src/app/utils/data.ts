export const convertToString = (value: unknown) => {
  if (typeof value === 'number') {
    return String(value);
  }
  return '';
};
