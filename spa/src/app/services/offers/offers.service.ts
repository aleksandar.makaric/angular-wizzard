import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject, tap } from 'rxjs';

import { environment } from '../../../../environments/environment';
import { IOffer } from '../../types';

@Injectable({
  providedIn: 'root',
})
export class OffersService {
  private _endpoint = `${environment.baseUrl}/offers`;

  private _offers$ = new Subject<IOffer[]>();

  get offers() {
    return this._offers$.asObservable();
  }

  constructor(private _http: HttpClient) {}

  getOffers() {
    return this._http.get<IOffer[]>(this._endpoint).pipe(
      tap((offers) => {
        this._offers$.next(offers);
      }),
    );
  }

  getOffer(id: number) {
    return this._http.get<IOffer>(`${this._endpoint}/${id}`);
  }

  createOffer() {
    return this._http.post<IOffer>(this._endpoint, {});
  }

  updateOffer(offer: IOffer) {
    return this._http.put<IOffer>(`${this._endpoint}/${offer.id}`, offer);
  }

  deleteOffer(id: number) {
    return this._http.delete<IOffer[]>(`${this._endpoint}/${id}`);
  }
}
