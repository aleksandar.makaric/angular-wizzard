export interface IOffersData {
  foundation: {
    label: string;
    values: string[];
  };
  activeToRetiredRatio: {
    label: string;
  };
  ageGroup: {
    label: string;
  };
  admissionGuidelines: {
    label: string;
  };
  retirementPlan: {
    label: string;
    values: string[];
  };
  useRetirementPlanTemplate: { label: string };
  retirementCategory: {
    label: string;
    values: number[];
  };
  lumpSumDeathBenefit: {
    label: string;
    values: string[];
  };
  savings: ISaving[];
  assignee: {
    label: string;
    values: string[];
  };
}

export interface ISaving {
  ageBracket: number;
  employee: number;
  employer: number;
  employeeDeduction: number;
  employeeSupplement: number;
}
