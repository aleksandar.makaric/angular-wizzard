const { STATUS } = require("./constants");

const CALCULATIONS = [
  {
    label: "Savings contribution (allocation according to broker taxes)",
    currency: "CHF",
    value: 500000,
  },
  {
    label: "Risk contribution (1.70% of the insured annual salary)",
    currency: "CHF",
    value: 100000,
  },
  {
    label: "Administrative expenses",
    currency: "CHF",
    value: 30000,
  },
  {
    label: "Total contribution per year at UWS 5.00%",
    currency: "CHF",
    value: 550000,
  },
];

const OFFERS = [
  {
    id: 123456,
    createdDtm: "2021-01-01T00:00:00.000Z",
    modifiedDtm: "2022-01-01T00:00:00.000Z",
    status: STATUS.INCOMPLETE,
    details: {
      foundation: "BLKP",
      firstName: "John",
      lastName: "Doe",
    },
  },
  {
    id: 234567,
    createdDtm: "2021-02-01T00:00:00.000Z",
    modifiedDtm: "2022-02-01T00:00:00.000Z",
    status: STATUS.INCOMPLETE,
    details: {
      foundation: "Swiss Life",
      firstName: "Jane",
      lastName: "Smith",
      ageGroup: true,
      retirementPlan: "99901.1",
      calculations: CALCULATIONS,
      useRetirementPlanTemplate: true,
      retirementPlanName: "Extraordinary retirement plan",
      retirementCategory: 3,
      retirementEntryAge: 55,
      retirementMinimumAge: 50,
      retirementMaximumAge: 60,
      lumpSumDeathBenefit: "BP (10x IV pension, AGH as of the reporting date)",
      fundRates: 30,
      riskRatesCategory1: 10,
    },
  },
  {
    id: 345678,
    createdDtm: "2021-03-01T00:00:00.000Z",
    modifiedDtm: "2022-03-01T00:00:00.000Z",
    status: STATUS.CALCULATED,
    details: {
      foundation: "Zurich",
      firstName: "Alice",
      lastName: "Johnson",
      retirementPlan: "99900.1",
      calculations: CALCULATIONS,
    },
  },
  {
    id: 456789,
    createdDtm: "2021-04-01T00:00:00.000Z",
    modifiedDtm: "2022-04-01T00:00:00.000Z",
    status: STATUS.CALCULATED,
    details: {
      foundation: "AXA",
      firstName: "Bob",
      lastName: "Williams",
      retirementPlan: "99901.1",
      calculations: CALCULATIONS,
    },
  },
  {
    id: 567890,
    createdDtm: "2021-05-01T00:00:00.000Z",
    modifiedDtm: "2022-05-01T00:00:00.000Z",
    status: STATUS.IN_REVIEW,
    details: {
      foundation: "Helvetia",
      firstName: "Charlie",
      lastName: "Brown",
      retirementPlan: "99901.1",
      calculations: CALCULATIONS,
      assignee: "Peter Jackson",
    },
  },
  {
    id: 678901,
    createdDtm: "2021-06-01T00:00:00.000Z",
    modifiedDtm: "2022-06-01T00:00:00.000Z",
    status: STATUS.IN_REVIEW,
    details: {
      foundation: "BLKP",
      firstName: "Diana",
      lastName: "Jones",
      retirementPlan: "99900.1",
      calculations: CALCULATIONS,
      assignee: "Tim Burton",
    },
  },
];

const OFFERS_DATA = {
  foundation: {
    label: "Foundation",
    values: ["BLKP", "Swiss Life", "Zurich", "AXA", "Helvetia"],
  },
  activeToRetiredRatio: { label: "Active to Retired Ratio" },
  ageGroup: { label: "Age Group" },
  admissionGuidelines: {
    label: "Does the client meet the admission guidelines?",
  },
  retirementPlan: {
    label: "Retirement Plan",
    values: ["99900.1", "99901.1"],
  },
  useRetirementPlanTemplate: { label: "Use Plan as a Template" },
  assignee: {
    label: "Choose an assignee to check your offer",
    values: ["Peter Jackson", "Tim Burton", "Steven Spielberg"],
  },
  retirementCategory: {
    label: "Category",
    values: [1, 2, 3],
  },
  lumpSumDeathBenefit: {
    label: "Lump-sum Death Benefit",
    values: [
      "BP (10x IV pension, AGH as of the reporting date)",
      "BP BLKB (AGH + 3x LOKO2)",
      "100% equity",
    ],
  },
  savings: [
    {
      ageBracket: 18,
      employee: 4,
      employer: 4,
      employeeDeduction: 4,
      employeeSupplement: 4,
    },
  ],
};

module.exports = { OFFERS, OFFERS_DATA, CALCULATIONS };
