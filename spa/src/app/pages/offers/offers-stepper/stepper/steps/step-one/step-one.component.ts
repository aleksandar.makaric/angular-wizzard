import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatStepper, MatStepperModule } from '@angular/material/stepper';
import { Subscription, switchMap } from 'rxjs';
import { OffersDataService, OffersService } from '../../../../../../services';
import {
  IOffer,
  IOffersData,
  THandleUpdateOffer,
  THandleUpdateOffersData,
  THandleUpdateStepper,
  THandleUpdateStepsCompleted,
  TStepperMode,
  TStepsCompleted,
} from '../../../../../../types';
import { isInputInvalid } from '../../../../../../utils';

@Component({
  selector: 'app-step-one',
  standalone: true,
  imports: [
    MatStepperModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  templateUrl: './step-one.component.html',
  styleUrls: ['./step-one.component.css', '../steps.css'],
})
export class StepOneComponent implements OnInit, OnDestroy {
  @Input() stepper!: MatStepper;
  @Input() handleUpdateStepper!: THandleUpdateStepper;
  @Input() mode!: TStepperMode;
  @Input() offer!: IOffer;
  @Input() handleUpdateOffer!: THandleUpdateOffer;
  @Input() offersData!: IOffersData;
  @Input() handleUpdateOffersData!: THandleUpdateOffersData;
  @Input() stepsCompleted!: TStepsCompleted;
  @Input() handleUpdateStepsCompleted!: THandleUpdateStepsCompleted;

  nextLoading = false;

  private _subscriptions: Subscription[] = [];

  constructor(
    private _formBuilder: FormBuilder,
    private _offersService: OffersService,
    private _offersDataService: OffersDataService
  ) {}

  firstStepForm = this._formBuilder.group({
    foundation: ['', Validators.required],
    firstName: ['', Validators.required],
    lastName: ['', Validators.required],
    activeToRetiredRatio: [false],
    ageGroup: [false],
    admissionGuidelines: [false],
  });

  get foundation() {
    return this.firstStepForm.get('foundation');
  }

  get firstName() {
    return this.firstStepForm.get('firstName');
  }

  get lastName() {
    return this.firstStepForm.get('lastName');
  }

  get isFoundationInvalid() {
    return isInputInvalid(this.foundation);
  }

  get isFirstNameInvalid() {
    return isInputInvalid(this.firstName);
  }

  get isLastNameInvalid() {
    return isInputInvalid(this.lastName);
  }

  get isNextButtonDisabled() {
    return this.firstStepForm.invalid || (this.mode === 'preview' && !this.stepsCompleted.first);
  }

  handleFirstStepSubmit(): void {
    if (this.firstStepForm.valid && (this.mode === 'new' || this.mode === 'edit') && !this.stepsCompleted.first) {
      this.nextLoading = true;
      // Enable in error case
      this.firstStepForm.disable();
      const offersSubscription = this._offersService
        .updateOffer({
          ...this.offer!,
          details: {
            ...this.offer.details,
            foundation: this.firstStepForm.value.foundation!,
            firstName: this.firstStepForm.value.firstName!,
            lastName: this.firstStepForm.value.lastName!,
            activeToRetiredRatio: this.firstStepForm.value.activeToRetiredRatio!,
            ageGroup: this.firstStepForm.value.ageGroup!,
            admissionGuidelines: this.firstStepForm.value.admissionGuidelines!,
          },
        })
        .pipe(
          switchMap((updatedOffer) => {
            this.handleUpdateOffer(updatedOffer);
            return this._offersDataService.getOffersData();
          })
        )
        .subscribe((newOffersData) => {
          this.nextLoading = false;
          this.handleUpdateOffersData(newOffersData);
          this.handleUpdateStepsCompleted({
            ...this.stepsCompleted,
            first: true,
          });
          this.handleUpdateStepper();
          this.stepper.next();
        });
      this._subscriptions.push(offersSubscription);
    } else {
      this.handleUpdateStepper();
      this.handleUpdateStepsCompleted({ ...this.stepsCompleted, first: true });
      this.stepper.next();
    }
  }

  ngOnInit(): void {
    const foundation = this.offer?.details?.foundation;
    const firstName = this.offer?.details?.firstName;
    const lastName = this.offer?.details?.lastName;

    this.firstStepForm.patchValue({
      foundation: foundation ?? '',
      firstName: firstName ?? '',
      lastName: lastName ?? '',
      activeToRetiredRatio: this.offer?.details?.activeToRetiredRatio ?? false,
      ageGroup: this.offer?.details?.ageGroup ?? false,
      admissionGuidelines: this.offer?.details?.admissionGuidelines ?? false,
    });

    if (foundation && firstName && lastName && this.mode !== 'edit') {
      this.handleUpdateStepsCompleted({ ...this.stepsCompleted, first: true });

      this.firstStepForm.disable();
    }

    if (this.mode === 'preview') {
      this.firstStepForm.disable();
    }
  }

  ngOnDestroy(): void {
    this._subscriptions.forEach((subscription) => subscription.unsubscribe());
  }
}
