const express = require("express");
const cors = require("cors");
const morgan = require("morgan");
const { OFFERS, OFFERS_DATA, CALCULATIONS } = require("./dummy_data.js");
const { STATUS } = require("./constants.js");

const app = express();

app.use(cors());
app.use(express.json());
app.use(morgan("dev"));

app.use((req, res, next) => {
  if (process.env.DELAY) {
    setTimeout(next, process.env.DELAY);
  } else {
    next();
  }
});

let offers = [...OFFERS];
let offersData = OFFERS_DATA;

// OFFERS
app.get("/offers", (req, res) => {
  res.status(200).json(offers);
});
app.get("/offers/:id", (req, res) => {
  const { id } = req.params;

  const offer = offers.find((offer) => offer.id === Number(id));

  res.status(200).json(offer);
});
app.post("/offers", (req, res) => {
  const id = Math.floor(100000 + Math.random() * 900000);
  const currentDateTime = new Date().toISOString();

  const offer = {
    id,
    createdDtm: currentDateTime,
    modifiedDtm: currentDateTime,
    status: STATUS.INCOMPLETE,
    details: {},
  };

  offers.unshift(offer);

  res.status(201).json(offer);
});
app.put("/offers/:id", (req, res) => {
  const { id } = req.params;
  const updatedOffer = req.body;

  updatedOffer.modifiedDtm = new Date().toISOString();

  if (
    updatedOffer.status === STATUS.CALCULATED &&
    updatedOffer.details.assignee
  ) {
    updatedOffer.status = STATUS.IN_REVIEW;
  } else if (
    updatedOffer.status === STATUS.INCOMPLETE &&
    updatedOffer.details.retirementPlan
  ) {
    updatedOffer.status = STATUS.CALCULATED;
    updatedOffer.details.calculations = CALCULATIONS;
  }

  offers = offers.map((offer) =>
    offer.id === Number(id) ? updatedOffer : offer
  );

  res.status(200).json(updatedOffer);
});
app.delete("/offers/:id", (req, res) => {
  const { id } = req.params;

  offers = offers.filter((offer) => offer.id !== Number(id));

  res.status(200).json({ message: `Offer with id ${id} deleted.` });
});
// ------------------------------

// OFFERS DATA
app.get("/offers-data", (req, res) => {
  res.status(200).json(offersData);
});

app.listen(3000, () => {
  console.log("Server is running on port 3000");
});
