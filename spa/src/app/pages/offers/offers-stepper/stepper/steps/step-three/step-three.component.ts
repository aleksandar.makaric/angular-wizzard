import { Component, Input } from '@angular/core';
import { MatStepperModule } from '@angular/material/stepper';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { CalculationTableComponent } from './calculation-table/calculation-table.component';
import { IOffer } from '../../../../../../types';

@Component({
  selector: 'app-step-three',
  standalone: true,
  imports: [
    MatStepperModule,
    MatIconModule,
    MatButtonModule,
    CalculationTableComponent,
  ],
  templateUrl: './step-three.component.html',
  styleUrls: ['./step-three.component.css', '../steps.css'],
})
export class StepThreeComponent {
  @Input() offer!: IOffer;
}
